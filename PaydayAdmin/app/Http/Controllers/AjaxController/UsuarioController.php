<?php

namespace App\Http\Controllers\AjaxController;

use App\Http\Controllers\Controller;
use App\Modelos\PersonaEmpleado;
use App\User;
use App\Utilidades\Constantes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    protected function AltaEmpleado(Request $request){
        $datos = $request->all();
        $this->ValidarDatosEmpleado($datos);
        if ($this->CrearUsuarioEmpleado($datos) != null){
            return response()->json(['respuesta' => 'Alta de usuario exitosa.'], 200);
        }else{
            return response()->json(['respuesta' => 'No se pudo realizar el alta.'], 500);
        }

    }

    protected function ModificarEmpleado(Request $request){

    }

    protected function ValidarDatosEmpleado($datos){
        return Validator::make($datos, [
            'email' => ['required', 'unique:Usuarios,Email','email:rfc,dns'],
            'nombre' => ['required'],
            'apellido' => ['required'],
            'documento' => ['required', 'numeric'],
            'sexo' => ['required'],
            'fechaNacimiento' => ['required'],
            'rol' => ['required'],
        ])->validate(); 
    }

    protected function CrearUsuarioEmpleado(array $datos){
        $usuario = User::create([
            'IdTipoUsuario' => Constantes::PersonaEmpleado,
            'Email' => $datos['email'],
            'Password' =>  Hash::make('WelcomePayDay2020'),
            'FechaRegistro' => Carbon::today(),
            'Puntos'=> 0,
            'ApiToken' => Str::random(100),
            'Activo' => 1,
        ]);

        $this->GenerarPersonaEmpleado($usuario->IdUsuario, $datos);

        return $usuario;
    }

    public function GenerarPersonaEmpleado(int $idUsuario,array $datos){
        $empleado = PersonaEmpleado::create([
            'IdUsuario' => $idUsuario,
            'Nombre' => $datos['nombre'],
            'Apellido' => $datos['apellido'],
            'Documento' => $datos['documento'],
            'Sexo' => $datos['sexo'],
            'FechaNacimiento' => $datos['fechaNacimiento'],
            'IdRol' => $datos['rol'],
        ]);
    }
}
