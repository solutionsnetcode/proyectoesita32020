<?php

namespace App\Http\Controllers;

use App\Modelos\Rol;
use App\Modelos\Servicio;
use App\User;
use Illuminate\Http\Request;

class ModulosController extends Controller
{
    protected function Dashboard(){
        return view("Dashboard");
    }

    protected function AltaEmpleado(){
        $roles = Rol::where('Activo', 1)->get();
        return view("Usuarios.Empleados.ABMUsuario", compact("roles"));
    }

    protected function ModificarEmpleado($idUsuario){
        $roles = Rol::where('Activo', 1)->get();
        $usuario = User::where('IdUsuario', $idUsuario)->with('Empleado')->first();

        return view("Usuarios.Empleados.ABMUsuario", compact("roles", "usuario"));
    }

    protected function ListadoEmpleados(){
        $empleados = User::where('IdTipoUsuario', 3)->with('Empleado')->get();
        return view("Usuarios.Empleados.ListadoEmpleados", compact("empleados"));
    }

    protected function ListadoClientes(){
        $equipo = User::where('IdTipoUsuario', 1)->with('PersonaFisica')->get();
        return view("Usuarios.Empleados.Listado", compact("equipo"));
    }

    protected function ListadoServicios(){
        $servicios = Servicio::all();
        return view("Servicios.ListadoServicios", compact("servicios"));
    }
    
    
}
