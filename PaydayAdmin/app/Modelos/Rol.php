<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Rol extends Model
{
    protected $table = 'Roles';
    protected $primaryKey = 'IdRol';

    protected $fillable = [
        'Rol', 'Activo'
    ];

    public function ListarPermisos(){
        return DB::table('Permisos')->join('PermisosDeRoles', 'PermisosDeRoles.IdPermiso', '=', 'Permisos.IdPermiso')->join('Roles', 'Roles.IdRol', '=', 'PermisosDeRoles.IdRol')->where('Roles.IdRol', $this->IdRol)->pluck('Permisos.Permiso');
    }
}
