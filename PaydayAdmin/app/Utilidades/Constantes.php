<?php

namespace App\Utilidades;

class Constantes
{

    // CONSTANTES QUE IDENTIFICAN EL TIPO DE USUARIO QUE ES
    public const PersonaFisica = 1;
    public const PersonaJuridica = 2;
    public const PersonaEmpleado = 3;
    ///////////////////////////////////////////////////////
}
    