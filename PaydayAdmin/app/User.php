<?php

namespace App;

use App\Modelos\PersonaEmpleado;
use App\Modelos\PersonaFisica;
use App\Modelos\PersonaJuridica;
use App\Modelos\Rol;
use App\Utilidades\Constantes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Psr\Http\Message\RequestInterface;

/** 
  * this class will hold functions for user interaction examples include user_pass(), user_username(), user_age(), user_regdate()
  * @author Jake Rocheleau jakerocheleau@gmail.com
  * @required settings.php
*/
class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'Usuarios';
    protected $primaryKey = 'IdUsuario';
    protected $remember_token = true;
    protected $guarded = ['IdUsuario'];


    protected $fillable = [
        'IdTipoUsuario', 'Email', 'Password', 'FechaRegistro','TokenActivacion', 'rememberToken', 'Puntos', 'Activo', 'ApiToken'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthPassword()
    {
        $pass = $this->Password;
        return $pass;
    }

    //RELACIONES// 

    public function Empleado(){
        return $this->hasOne(PersonaEmpleado::class, 'IdUsuario' ,'IdUsuario');
    }

    public function PersonaFisica(){
        return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
    }


    public function DatosUsuario(){
        if ($this->IdTipoUsuario == Constantes::PersonaFisica){
            return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
        }
        elseif($this->IdTipoUsuario == Constantes::PersonaJuridica){
            return $this->hasOne(PersonaJuridica::class, 'IdUsuario' ,'IdUsuario');
        }  
        else{
            return $this->hasOne(PersonaEmpleado::class, 'IdUsuario' ,'IdUsuario');
        }   
    }
}
