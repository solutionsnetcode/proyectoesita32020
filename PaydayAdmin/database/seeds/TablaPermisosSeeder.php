<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TablaPermisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permisos = array(
            "1" => array(
                'permiso' => 'Alta Usuario',
            ),
            "2" => array(
                'permiso' => 'Modificacion Usuario',
            ),
            "3" => array(
                'permiso' => 'Baja Usuario',
            ),
            "4" => array(
                'permiso' => 'Alta Producto Catalogo',
            ),
            "5" => array(
                'permiso' => 'Modificacion Producto Catalogo',
            ),
            "6" => array(
                'permiso' => 'Baja Producto Catalogo',
            ),
            "7" => array(
                'permiso' => 'Alta Medio de Pago',
            ),
            "8" => array(
                'permiso' => 'Modificacion Medio de Pago',
            ),
            "9" => array(
                'permiso' => 'Baja Medio de Pago',
            ),
            "10" => array(
                'permiso' => 'Alta Proveedor',
            ),
            "11" => array(
                'permiso' => 'Modificacion Proveedor',
            ),
            "12" => array(
                'permiso' => 'Baja Proveedor',
            ),
            "13" => array(
                'permiso' => 'Alta Articulo',
            ),
            "14" => array(
                'permiso' => 'Modificacion Articulo',
            ),
            "15" => array(
                'permiso' => 'Baja Articulo',
            ),
            "16" => array(
                'permiso' => 'Alta Publicidad',
            ),
            "17" => array(
                'permiso' => 'Modificacion Publicidad',
            ),
            "18" => array(
                'permiso' => 'Baja Publicidad',
            ),
            "19" => array(
                'permiso' => 'Alta Rol',
            ),
            "20" => array(
                'permiso' => 'Modificacion Rol',
            ),
            "21" => array(
                'permiso' => 'Baja Rol',
            ),
            "22" => array(
                'permiso' => 'Listar Articulos',
            ),
            "23" => array(
                'permiso' => 'Listar Pagos',
            ),
            "24" => array(
                'permiso' => 'Listar Clientes',
            ),
            "25" => array(
                'permiso' => 'Listar Publicidades',
            ),
            "26" => array(
                'permiso' => 'Listar Productos Catalogo',
            ),

        );

        foreach ($permisos as $key => $permiso) {
            DB::table('Permisos')->insert([
             'Permiso' => $permiso['permiso'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } 


    }
}
