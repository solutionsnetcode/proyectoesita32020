<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TablaUsuarioEmpleadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = array(
            "1" => array(
                'tipoUsuario' => 3,
                'email' => 'sistemas@netcodesolutions.com',
                'password' => Hash::make('$istema$00'),
                'fechaRegistro' => Carbon::now()->format('Y-m-d'),
                'activo' => 1,
                'apiToken' => '213dXyNOTOKEN123$#23fdSA'
            ),
        );

        $datos = array(
            "1" => array(
                'idUsuario' => 1,
                'nombre' => 'Sistema',
                'apellido' => 'Informatico',
                'documento' => '00000000',
                'sexo' => 'Otro',
                'fechaNacimiento' => Carbon::now()->format('Y-m-d'),
                'idRol' => '4',
            ),
        );

        foreach ($usuarios as $key => $usuario) {
            DB::table('Usuarios')->insert([
             'IdTipoUsuario' => $usuario['tipoUsuario'],
             'Email' => $usuario['email'],
             'Password' => $usuario['password'],
             'FechaRegistro' => $usuario['fechaRegistro'],
             'Activo' => $usuario['activo'],
             'ApiToken' => $usuario['apiToken'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } 

        foreach ($datos as $key => $dato) {
            DB::table('PersonasEmpleados')->insert([
             'IdUsuario' => $dato['idUsuario'],
             'Nombre' => $dato['nombre'],
             'Apellido' => $dato['apellido'],
             'Documento' => $dato['documento'],
             'Sexo' => $dato['sexo'],
             'FechaNacimiento' => $dato['fechaNacimiento'],
             'IdRol' => $dato['idRol'],
             'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
             'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        } 


    }
}
