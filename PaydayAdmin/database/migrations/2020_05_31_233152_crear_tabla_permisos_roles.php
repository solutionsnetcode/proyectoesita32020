<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPermisosRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PermisosDeRoles', function (Blueprint $table) {
            $table->unsignedTinyInteger("IdRol");
            $table->unsignedTinyInteger("IdPermiso");
            $table->timestamps();


            $table->primary(['IdRol', 'IdPermiso']);
            $table->foreign('IdRol', 'FK_Rol_PermisoRol')->references('IdRol')->on('Roles')->onDelete('restrict');
            $table->foreign('IdPermiso', 'FK_Permiso_PermisoUsuario')->references('IdPermiso')->on('Permisos')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PermisosDeRoles');
    }
}
