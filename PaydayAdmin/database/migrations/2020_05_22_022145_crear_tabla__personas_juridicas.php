<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaPersonasJuridicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ////////////////////////////////////////////////////////////
        //      TABLA QUE CONTIENE LOS USUARIOS DEL SISTEMA       //
        ////////////////////////////////////////////////////////////   
        Schema::create('PersonasJuridicas', function (Blueprint $table) {
            $table->increments("IdPersonaJuridica");
            $table->unsignedInteger("IdUsuario")->unique();
            $table->string("RazonSocial", 100);
            $table->string("NombreFantasia", 100);
            $table->string("Rut", 30)->unique();
            $table->enum("FormaJuridica",["UNI", "SRL", "SA", "Otra"]);
            $table->date("FechaConformacion");
            $table->timestamps();

            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Usuario_PersonaJuridica')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PersonasJuridicas');
    }
}
