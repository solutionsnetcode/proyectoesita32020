<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaTelefonosUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TelefonosUsuarios', function (Blueprint $table) {
            $table->increments("IdTelefono");
            $table->unsignedInteger("IdUsuario");
            $table->string("Telefono", 40);
            $table->boolean("Activo"); 
            $table->timestamps();

            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdUsuario', 'FK_Telefono_Usuario')->references('IdUsuario')->on('Usuarios')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TelefonosUsuarios');
    }
}
