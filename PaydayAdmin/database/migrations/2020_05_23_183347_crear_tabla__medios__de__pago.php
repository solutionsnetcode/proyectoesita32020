<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaMediosDePago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MediosDePago', function (Blueprint $table) {
            $table->tinyIncrements("IdMedioPago");
            $table->string("Nombre", 50);
            $table->string("Descripcion", 200);
            $table->string("UrlPago", 200);
            $table->boolean("Activo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MediosDePago');
    }
}
