<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        ////////////////////////////////////////////////////////////
        //      TABLA QUE CONTIENE LOS USUARIOS DEL SISTEMA       //
        ////////////////////////////////////////////////////////////
        Schema::create('Usuarios', function (Blueprint $table) {
            $table->increments("IdUsuario");
            $table->unsignedTinyInteger("IdTipoUsuario");
            $table->string("Email",40)->unique();
            $table->string("Password", 60);
            $table->date("FechaRegistro");
            $table->string('TokenActivacion', 40)->nullable();
            $table->rememberToken()->nullable();
            $table->unsignedInteger("Puntos")->default(0);
            $table->boolean("Activo");
            $table->string("ApiToken")->unique()->nullable();
            $table->timestamps();


            ///////////////////
            //CLAVES FORANEAS//
            ///////////////////
            $table->foreign('IdTipoUsuario', 'FK_TipoUsuario_Usuario')->references('Id')->on('TiposUsuario')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Usuarios');
    }
}
