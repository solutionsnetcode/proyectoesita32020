<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Rutas Web
|--------------------------------------------------------------------------
|
| Encontraremos todas las rutas del sistema de administracion
| Todas se encuentran protegidas por el middleware que controla la sesion
|
*/

//Pagina de Iniciar Sesion
Route::get('/inicioSesion', 'LoginController@IniciarSesionView');

//Accion de iniciar sesion
Route::get('/iniciarSesion', 'LoginController@IniciarSesion')->name('IniciarSesionAccion');



Route::middleware('VerificarSesion')->group(function () {
    Route::get('/', 'ModulosController@Dashboard');
    Route::get('/cerrarSesion', 'LoginController@CerrarSesion')->name('CerrarSesion');
    Route::get('/dashboard', 'ModulosController@Dashboard')->name('Dashboard');

    Route::get('/agregarEmpleado','ModulosController@AltaEmpleado')->name('AltaEmpleado');
    Route::get('/modificarEmpleado/{idUsuario}', 'ModulosController@ModificarEmpleado')->name('ModificarEmpleado');
    Route::get('/listadoEmpleados', 'ModulosController@ListadoEmpleados')->name('ListadoEmpleados');

    Route::get('/listadoServicios', 'ModulosController@ListadoServicios')->name('ListadoServicios');



    // PETICIONES POR AJAX //////////////////////
    Route::post('/AltaEmpleadoAjax', 'AjaxController\UsuarioController@AltaEmpleado');

    ////////////////////////////////////////////
});



