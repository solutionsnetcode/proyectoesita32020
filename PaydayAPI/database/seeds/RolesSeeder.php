<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array(
            "1" => array(
                'rol' => 'Usuario',
                'activo' => 1,
                'soloempleado' => 0
            ),
        );

        foreach ($roles as $key => $rol) {

            DB::table('Roles')->insert([
             'Rol' => $rol['rol'],
             'Activo' => $rol['activo'],
             'SoloEmpleado' => $rol['soloempleado'],
            ]);

        }
    }
}
