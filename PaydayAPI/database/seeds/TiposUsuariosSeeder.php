<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TiposUsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tiposUsuarios = array(
            "1" => array(
                'tipo' => 'Persona Fisica',
                'activo' => 1,
            ),
            "2" => array(
                'tipo' => 'Persona Juridica',
                'activo' => 1,
            ),
            "3" => array(
                'tipo' => 'Persona Empleado',
                'activo' => 1,
            ),
        );

        foreach ($tiposUsuarios as $key => $tipos) {

            DB::table('TiposUsuarios')->insert([
             'TipoUsuario' => $tipos['tipo'],
             'Activo' => $tipos['activo'],
            ]);

        }
    }
}