<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(TiposUsuariosSeeder::class);//--------------> Tipos de Usuario
        $this->call(RolesSeeder::class);//----------------------> Roles de Usuario
        $this->call(UsuarioSeeder::class);//--------------------> Usuario Base
    }
}
