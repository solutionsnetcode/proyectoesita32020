<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AutenticacionController
{
    public function IniciarSesion(Request $request){
        $usuario = User::where('Email', $request->email)->first();
        
        if($usuario != null){
            if ($usuario->Password == $request->password){      
                return response()->json(['usuario' => $usuario->Email, 'token' => $this->GenerarToken($usuario)], 200);
            }
            
        }
        else{
            return response()->json(['respuesta' => 'Credenciales erroneas.'], 500);
        }
        
    }

    public function GenerarToken(User $usuario){
        $token = Str::random(50);
        $usuario->ApiToken = $token;
        $usuario->save();

        return $token;
    }


}
