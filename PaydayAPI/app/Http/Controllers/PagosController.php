<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmacionPago;
use App\Modelos\Pago;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PagosController extends Controller
{
    
    protected function AgregarPago(Request $request){
        $datos = $request->all();
        $this->ValidarDatosPago($datos);
        $pago = $this->AltaPago($datos);
        $this->AumentarPuntaje($pago);
        $this->NotificarPago($pago);
    }



    private function ValidarDatosPago(array $datos){
        $validacion = Validator::make($datos, [
            'monto' => ['required', 'numeric'],
            'medioPago' => ['required', 'numeric', 'exists:MediosDePago,IdMedioPago'], 
            'moneda' => ['required', 'numeric', 'exists:Monedas,IdMoneda'],
            'servicio' => ['required', 'numeric', 'exists:Servicios,IdServicio'],
            'usuario' => ['required', 'exists:Usuarios,Email'],
            'numeroTarjeta' => ['required'],
            'ccv' => ['required'],
        ]);
        if ($validacion->errors()->count() > 0){
            return response()->json(['errores' => $validacion->errors()]);
        }
    }

    private function AltaPago(array $datos){
        $usuario = User::where('Email', $datos['usuario'])
        return Pago::create([
            'IdUsuario' => $usuario->IdUsuario,
            'IdMoneda' => $datos['moneda'],
            'MontoPago' =>  $datos['monto'],
            'IdServicio' => $datos['servicio'],
            'FechaGenera'=> Carbon::today(),
            'FechaPaga' => Carbon::today(),
            'IdMedioPago' => $datos['medioPago'],
            'Anulado' => 0,
        ]);
    }

    private function AumentarPuntaje(Pago $pago){
        $usuario = User::find($pago->IdUsuario);
        $usuario->Puntos += $pago->PuntajeGanado();
        $usuario->save();
    }

    private function NotificarPago(Pago $pago){
        Mail::to($pago->Usuario->Email)->send(new ConfirmacionPago($pago));
    }




}
