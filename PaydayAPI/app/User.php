<?php

namespace App;

use App\Modelos\Pago;
use App\Modelos\PersonaEmpleado;
use App\Modelos\PersonaFisica;
use App\Modelos\Servicio;
use App\Modelos\Tarjeta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'Usuarios';
    protected $primaryKey = 'IdUsuario';

    protected $fillable = [
        'IdTipoUsuario', 'Email', 'Password', 'FechaRegistro','TokenActivacion', 'rememberToken', 'Puntos', 'Activo', 'ApiToken'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAuthPassword(){
        $pass = $this->Password;
        return $pass;
    }

    public function getToken(){
        return Str::random(100);
    }

      //RELACIONES// 

    public function Empleado(){
        return $this->hasOne(PersonaEmpleado::class, 'IdUsuario' ,'IdUsuario');
    }

    public function PersonaFisica(){
        return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
    }

    public function Servicios(){
        return $this->belongsToMany(Servicio::class, 'serviciosagendados', 'IdUsuario', 'IdServicio');
    }

    public function Tarjetas(){
        return $this->hasMany(Tarjeta::class, 'IdUsuario', 'IdUsuario');
    }

    public function Pagos(){
        return $this->hasMany(Pago::class, 'IdUsuario', 'IdUsuario');
    }

    public function GenerarmeToken(){

    }
}
