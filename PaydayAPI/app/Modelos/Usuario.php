<?php

namespace App\Modelos;

use App\Utilidades\Constantes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Psr\Http\Message\RequestInterface;
/** 
  * Esta clase es la utilizada para utenticarse en el sistema y es la que contiene toda la inforamcion del usuario.
  * @author NetCode Solutions solutionsnetcode@gmail.com
*/
class Usuario extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'Usuarios';
    protected $primaryKey = 'IdUsuario';
    protected $remember_token = true;
    protected $guarded = ['IdUsuario'];


    protected $fillable = [
        'IdTipoUsuario', 'Email', 'Password', 'FechaRegistro','TokenActivacion', 'rememberToken', 'Puntos', 'Activo', 'ApiToken'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthPassword()
    {
        $pass = $this->Password;
        return $pass;
    }

    
    public function Rol(){
        return $this->hasOne(Rol::class, 'IdRol' ,'IdUsuario');
    }

    public function DatosUsuario(){
        if ($this->IdTipoUsuario == Constantes::PersonaFisica){
            return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
        }
        elseif($this->IdTipoUsuario == Constantes::PersonaJuridica){
            return $this->hasOne(PersonaJuridica::class, 'IdUsuario' ,'IdUsuario');
        }  
        else{
            return $this->hasOne(PersonaEmpleado::class, 'IdUsuario' ,'IdUsuario');
        }   
    }
}
