<?php

namespace App\Mail;

use App\Modelos\Pago;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ConfirmacionPago extends Mailable
{
    use Queueable, SerializesModels;

    public Pago $pago;

    public function __construct($pago)
    {
        $this->pago = $pago;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('Pagos@Payday.com', 'Payday')
        ->subject('Confirmación de Pago')
         ->view('Mails.RecuperarContraseña');
    }
}
