<?php

namespace Tests\Unit;

use App\Mail\RecuperarContraseña;
use App\User;
use Illuminate\Support\Facades\Mail;
use PHPUnit\Framework\TestCase;
use Tests\TestCase as TestsTestCase;

class RecuperarContraseñaTest extends TestsTestCase
{
    /**
     * @test
     */
    public function GeneracionTokenActivacion()
    {
        $usuario = User::find(2);
        $usuario->ApiToken = $usuario->getToken();
        $usuario->save();

        $this->assertNotEmpty(User::find(2)->ApiToken);
    }

    /**
     * @test
     */
    public function VaciarTokenActivacion()
    {
        $usuario = User::find(2);
        $usuario->ApiToken = null;
        $usuario->save();

        $this->assertEmpty(User::find(2)->ApiToken);
    }



}
