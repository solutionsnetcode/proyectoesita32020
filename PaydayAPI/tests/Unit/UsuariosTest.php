<?php

namespace Tests\Unit;

use App\Modelos\PersonaFisica;
use App\User;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UsuariosTest extends TestCase
{

    /**
     * A basic unit test example.
     *
     * @test
     */
    public function AltaUsuario(){
        $faker = Factory::create();

        $nuevoUsuario = User::create([
            'IdTipoUsuario' => random_int(1,2),
            'Email' => $faker->email,
            'Password' =>  Hash::make("random123"),
            'FechaRegistro' => carbon::now(),
            'Puntos'=> 0,   
            'Activo' => 1,
        ]);

        $this->assertDatabaseHas('Usuarios', [
            'Email' => "$nuevoUsuario->Email",
        ]);
    }

    /**
     * @test
     */
    public function AltaPersonaFisica(){
        $faker = Factory::create();
        $nuevoUsuario = User::create([
            'IdTipoUsuario' => random_int(1,2),
            'Email' => $faker->email,
            'Password' =>  Hash::make("random123"),
            'FechaRegistro' => carbon::now(),
            'Puntos'=> 0,
            'Activo' => 1,
        ]);

        $nuevaPersona = PersonaFisica::create([
            'IdUsuario' => $nuevoUsuario->IdUsuario,
            'Nombre' => $faker->name,
            'Apellido' => $faker->lastName,
            'Documento' => random_int(50000000, 59000000),
            'Sexo'=> "Masculino",
            'FechaNacimiento' => carbon::now(),
        ]);

        $this->assertDatabaseHas('PersonasFisicas', [
            'IdUsuario' => "$nuevoUsuario->IdUsuario",
        ]);
    }

    /**
     * @test
     */
    public function ModificacionEmailUsuario(){
        $faker = Factory::create();

        $nuevoEmail = $faker->email;
        $usuario = User::find(2);
        $usuario->Email = $nuevoEmail;

        $usuario->save();

        $usuario = User::find(2);
        $this->assertEquals($nuevoEmail, $usuario->Email);
    }




}
