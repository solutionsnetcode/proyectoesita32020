<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Rutas de la api de Payday que permiten incertar y acceder a la informacion almacenada en nuestra BD
|
| Autor: NetCode Solutions solutionsnetcode@gmail.com
|
*/

// Utilizada para autenticacion del sistema.
Route::get('login', 'AutenticacionController@IniciarSesion');

// Devuelve una lista de usuarios o un usuario en caso de que se le pase un parametro (id de usuario)
Route::get('user/{id?}', 'UsuarioController@ObtenerUsuarios');

// Registra un usuario en el sistema
Route::post('user/registro', 'UsuarioController@AltaUsuario');

// Modifica lo datos de un usuario creado
Route::put('user/modify', 'UsuarioController@ModificacionUsuario');

// Permite eliminar un usuario del sistema (Baja logica)
Route::delete('user/deleted', 'UsuarioController@BajaUsuario');

// Envia un correo con un token que permite acceder al cambio de contraseña
Route::get('recuperarContraseña', 'RecuperarContraseñaController@RecuperarContraseña');

// Recibe un token y si este es correcto nos envia a la ventana de cambio de contraseña
Route::get('recuperarContraseña/{token}', 'RecuperarContraseñaController@RecuperarContraseñaView')->name('recuperarContraseña');

// Realiza el cambio de contraseña del usuario
Route::post('recuperarContraseña', 'RecuperarContraseñaController@CambioDeContraseña');

// Lista los servicios activos del sistema o los agendados por el usuario del que se envia el ID
Route::get('servicios/{idUsuario?}', 'ServiciosController@ListarServicios');

// Lista los metodos de pagos del sistema
Route::get('metodosDePago', 'MetodosPagosController@ListarMetodosDePago');

// Agrega una tarjeta a un usuario especifico
//Route::post('nuevaTarjeta', 'TarjetasUsuariosController@AgregarTarjeta');

// Lista las tarjetas que el usuario agrego o una tarjeta en particular.
//Route::get('tarjetasUsuario/{idUsuario}/{idTarjeta?}', 'TarjetasUsuariosController@ListarTarjetas');

// Registra un pago en la plataforma
Route::post('nuevoPago', 'PagosController@AgregarPago');

// Lista los pagos realizados por el usuario
Route::get('pago/{idUsuario}', 'PagosController@ListarPagos');




