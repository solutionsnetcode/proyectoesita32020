$(document).ready(function () {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url:'http://localhost/PaydayAPI/public/servicios',
        type:'get',
        dataType: "json",
        success: function (response) {
            var texto = "Puedes realizar tus pagos de ";
            $.each(response['servicios'], function( i, l ){
                texto += " " + l['Nombre'] + ",";
              });
            $("#txtPagos").html(texto + " entre otros.");
    },
    statusCode: {              
        404: function() {
        alert("El servicio esta apagado.");
        }
    },
    error:function(x,xs,xt){
        alert('El usuario no esta registrado');
        }
    });



});